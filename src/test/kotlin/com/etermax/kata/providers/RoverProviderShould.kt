package com.etermax.kata.providers

import com.etermax.kata.domain.model.Direction
import com.etermax.kata.domain.model.Position
import com.etermax.kata.domain.model.Rover
import com.etermax.kata.domain.repository.RoverRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class RoverProviderShould {

    private lateinit var roverRepository: RoverRepository
    private lateinit var provider: RoverProvider

    @Before
    fun before() {
        roverRepository = mock()
        provider = RoverProvider(roverRepository)
    }

    @Test
    fun `return a new rover`() {
        val rover = Rover(Position(), roverRepository)

        val result = provider.provide()

        assertEquals(rover, result)
    }

    @Test
    fun `return an existing rover`() {
        val rover = Rover(Position(1, 5, Direction.WEST), roverRepository)
        given(roverRepository.get()).willReturn(rover)

        val result = provider.provide()

        assertEquals(rover, result)
    }
}