package com.etermax.kata.infrastructure.repository

import com.etermax.kata.domain.repository.RoverRepository
import com.etermax.kata.domain.repository.RoverRepositoryShould
import com.etermax.kata.infrastructure.InMemoryRoverRepository

class InMemoryRoverRepositoryShould : RoverRepositoryShould() {
    override fun initialize(): RoverRepository = InMemoryRoverRepository()
}