package com.etermax.kata.domain.model

import com.etermax.kata.domain.repository.RoverRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals

@RunWith(JUnitParamsRunner::class)
class RoverShould {

    private val initialPosition = Position(0, 0, Direction.NORTH)
    private lateinit var repository: RoverRepository
    private lateinit var rover: Rover

    @Before
    fun before() {
        repository = mock()
        rover = Rover(initialPosition, repository)
    }

    @Test
    fun `create a rover on specific position`() {
        assertEquals(initialPosition, rover.position)
    }

    @Test
    @Parameters(
        "0, 1, NORTH",
        "0, -1, SOUTH",
        "1, 0, EAST",
        "-1, 0, WEST"
    )
    fun `move rover forward`(expectedX: Int, expectedY: Int, direction: Direction) {
        val initialPosition = Position(0, 0, direction)
        rover = Rover(initialPosition, repository)

        val result = rover.move(Movement.FORWARD)
        val position = result.position

        assertEquals(expectedX, position.x)
        assertEquals(expectedY, position.y)
        assertEquals(direction, position.direction)
    }

    @Test
    @Parameters(
        "0, -1, NORTH",
        "0, 1, SOUTH",
        "-1, 0, EAST",
        "1, 0, WEST"
    )
    fun `move rover backward`(expectedX: Int, expectedY: Int, direction: Direction) {
        val initialPosition = Position(0, 0, direction)
        rover = Rover(initialPosition, repository)

        val result = rover.move(Movement.BACKWARD)
        val position = result.position

        assertEquals(expectedX, position.x)
        assertEquals(expectedY, position.y)
        assertEquals(direction, position.direction)
    }

    @Test
    @Parameters(
        "NORTH, EAST",
        "EAST, SOUTH",
        "SOUTH, WEST",
        "WEST, NORTH"
    )
    fun `turn rover right`(initialDirection: Direction, expectedDirection: Direction) {
        val initialPosition = Position(0, 0, initialDirection)
        val rover = Rover(initialPosition, repository)

        val result = rover.move(Movement.TURN_RIGHT)
        val position = result.position

        assertEquals(0, position.x)
        assertEquals(0, position.y)
        assertEquals(expectedDirection, position.direction)
    }

    @Test
    @Parameters(
        "NORTH, WEST",
        "WEST, SOUTH",
        "SOUTH, EAST",
        "EAST, NORTH"
    )
    fun `turn rover left`(initialDirection: Direction, expectedDirection: Direction) {
        val initialPosition = Position(0, 0, initialDirection)
        val rover = Rover(initialPosition, repository)

        val result = rover.move(Movement.TURN_LEFT)
        val position = result.position

        assertEquals(0, position.x)
        assertEquals(0, position.y)
        assertEquals(expectedDirection, position.direction)
    }

    @Test
    fun `store resultant rover into repository`() {
        val result = rover.move(Movement.FORWARD)

        verify(repository, times(1)).store(result)
    }
}