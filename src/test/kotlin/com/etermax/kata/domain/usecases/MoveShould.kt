package com.etermax.kata.domain.usecases

import com.etermax.kata.domain.model.*
import com.etermax.kata.providers.RoverProvider
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException
import kotlin.test.assertTrue

class MoveShould {

    private lateinit var roverProvider: RoverProvider
    private lateinit var rover: Rover
    private val movements = listOf("f", "f", "b")

    private lateinit var move: Move

    @Before
    fun before() {
        roverProvider = mock()
        rover = mock()
        move = Move(roverProvider)
    }


    @Test
    fun `return rover position and movements made`() {
        given(roverProvider.provide()).willReturn(rover)
        given(rover.move(any())).willReturn(rover)
        given(rover.position).willReturn(Position(1,1, Direction.NORTH))

        val result = move(movements)

        val inOrder = inOrder(rover)
        inOrder.verify(rover, times(2)).move(Movement.FORWARD)
        inOrder.verify(rover).move(Movement.BACKWARD)
        assertTrue(result is MoveResult.Success)
    }

    @Test
    fun `return rover position, movements made and error`() {
        given(roverProvider.provide()).willReturn(rover)
        given(rover.move(any())).willReturn(rover).willReturn(rover).willThrow(RuntimeException("Obstacle found"))
        given(rover.position).willReturn(Position(1,1, Direction.NORTH))

        val result = move(movements)

        val inOrder = inOrder(rover)
        inOrder.verify(rover, times(2)).move(Movement.FORWARD)
        inOrder.verify(rover).move(Movement.BACKWARD)
        assertTrue(result is MoveResult.Failure)
    }

}