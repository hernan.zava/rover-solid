package com.etermax.kata.domain.repository

import com.etermax.kata.domain.model.Position
import com.etermax.kata.domain.model.Rover
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

abstract class RoverRepositoryShould {

    private lateinit var repository: RoverRepository

    protected abstract fun initialize(): RoverRepository

    @Before
    fun before() {
        repository = initialize()
    }

    @Test
    fun `get stored rover`() {
        val rover = Rover(Position(), repository)
        repository.store(rover)

        val result = repository.get()

        assertEquals(result, rover)
    }
}