package com.etermax.kata.resources

import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.etermax.kata.domain.model.Direction
import com.etermax.kata.domain.model.MoveResult
import com.etermax.kata.domain.model.Movement
import com.etermax.kata.domain.model.Position
import com.etermax.kata.domain.usecases.Move
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import spark.Request
import spark.Response
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_OK
import kotlin.test.assertEquals

class MoveResourceShould {

    private lateinit var resource: MoveResource
    private lateinit var request: Request
    private lateinit var response: Response
    private lateinit var move: Move

    private val movements = listOf("f", "r", "b", "l")

    private val movementsResult = listOf(Movement.FORWARD, Movement.TURN_RIGHT, Movement.BACKWARD, Movement.TURN_LEFT)
    private val movementsFailureResult = listOf(Movement.FORWARD, Movement.TURN_RIGHT)

    private val newPosition = JsonObject().add("x", 1).add("y", 0).add("direction", "N")
    private val roverExpectedNewPosition = JsonObject().add("roverPosition", newPosition)
        .add("roverMovements", JsonArray().add("f").add("r").add("b").add("l"))
        .toString()

    private val newPositionOnFailed = JsonObject().add("x", 1).add("y", 1).add("direction", "W")
    private val roverExpectedNewPositionOnFailed = JsonObject().add("roverPosition", newPositionOnFailed)
        .add("roverMovements", JsonArray().add("f").add("r"))
        .add("error", "Obstacle found at x:1, y:0")
        .toString()

    @Before
    fun before() {
        request = mock()
        response = mock()
        move = mock()
        resource = MoveResource(move)
    }

    @Test
    fun `return rover new position on move`() {
        val listOfMovements = JsonArray().add("f").add("r").add("b").add("l")
        val jsonMovements = JsonObject().add("movements", listOfMovements)
        given(request.body()).willReturn(jsonMovements.toString())
        given(move(movements)).willReturn(MoveResult.Success(Position(1, 0, Direction.NORTH), movementsResult))


        val roverNewPosition = resource.move(request, response)

        verify(move).invoke(movements)
        verify(response).status(HTTP_OK)
        assertEquals(roverExpectedNewPosition, roverNewPosition)
    }

    @Test
    fun `return rover new position and error`() {
        val listOfMovements = JsonArray().add("f").add("r").add("b").add("l")
        val jsonMovements = JsonObject().add("movements", listOfMovements)
        given(request.body()).willReturn(jsonMovements.toString())
        given(move(movements)).willReturn(MoveResult.Failure(Position(1, 1, Direction.WEST), movementsFailureResult,"Obstacle found at x:1, y:0"))

        val roverNewPosition = resource.move(request, response)

        verify(move).invoke(movements)
        verify(response).status(HTTP_BAD_REQUEST)
        assertEquals(roverExpectedNewPositionOnFailed, roverNewPosition)
    }
}