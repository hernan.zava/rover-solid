package com.etermax.kata.domain.repository

import com.etermax.kata.domain.model.Rover

interface RoverRepository {
    fun store(rover: Rover)
    fun get(): Rover?
}
