package com.etermax.kata.domain.model

import com.etermax.kata.domain.repository.RoverRepository

data class Rover(val position: Position = Position(), val repository: RoverRepository) {

    fun move(movement: Movement): Rover {
        return when (movement) {
            Movement.FORWARD -> moveForward()
            Movement.BACKWARD -> moveBackward()
            Movement.TURN_RIGHT -> turnRight()
            Movement.TURN_LEFT -> turnLeft()
        }.also { repository.store(it) }

    }

    private fun turnLeft(): Rover {
        return when (position.direction) {
            Direction.NORTH -> Rover(position.copy(direction = Direction.WEST), repository)
            Direction.WEST -> Rover(position.copy(direction = Direction.SOUTH), repository)
            Direction.SOUTH -> Rover(position.copy(direction = Direction.EAST), repository)
            Direction.EAST -> Rover(position.copy(direction = Direction.NORTH), repository)
        }
    }

    private fun turnRight(): Rover {
        return when (position.direction) {
            Direction.NORTH -> Rover(position.copy(direction = Direction.EAST), repository)
            Direction.EAST -> Rover(position.copy(direction = Direction.SOUTH), repository)
            Direction.SOUTH -> Rover(position.copy(direction = Direction.WEST), repository)
            Direction.WEST -> Rover(position.copy(direction = Direction.NORTH), repository)
        }
    }

    private fun moveBackward(): Rover {
        return when (position.direction) {
            Direction.NORTH -> Rover(position.copy(y = position.y - 1), repository)
            Direction.SOUTH -> Rover(position.copy(y = position.y + 1), repository)
            Direction.EAST -> Rover(position.copy(x = position.x - 1), repository)
            Direction.WEST -> Rover(position.copy(x = position.x + 1), repository)
        }
    }

    private fun moveForward(): Rover {
        return when (position.direction) {
            Direction.NORTH -> Rover(position.copy(y = position.y + 1), repository)
            Direction.SOUTH -> Rover(position.copy(y = position.y - 1), repository)
            Direction.EAST -> Rover(position.copy(x = position.x + 1), repository)
            Direction.WEST -> Rover(position.copy(x = position.x - 1), repository)
        }
    }

}
