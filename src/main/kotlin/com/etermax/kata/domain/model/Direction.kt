package com.etermax.kata.domain.model

enum class Direction(val value: String) {
    NORTH("N"),
    SOUTH("S"),
    EAST("E"),
    WEST("W")
}
