package com.etermax.kata.domain.model

sealed class MoveResult {
    data class Success(val position: Position, val movements: List<Movement>): MoveResult()
    data class Failure(val position: Position, val movements: List<Movement>, val error: String): MoveResult()
}
