package com.etermax.kata.domain.model

enum class Movement(val value: String) {
    TURN_LEFT("l"),
    TURN_RIGHT("r"),
    FORWARD("f"),
    BACKWARD("b");

    companion object {
        private val mapping = values().associateBy(Movement::value)
        fun fromValue(value: String) = mapping[value] ?: error("Look up failed for ${this.javaClass.declaringClass}")
    }
}
