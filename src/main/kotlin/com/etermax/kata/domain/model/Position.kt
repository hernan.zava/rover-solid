package com.etermax.kata.domain.model

data class Position(val x: Int = 0, val y: Int = 0, val direction: Direction = Direction.NORTH)
