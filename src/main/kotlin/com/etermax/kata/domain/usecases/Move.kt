package com.etermax.kata.domain.usecases

import com.etermax.kata.domain.exceptions.ObstacleFoundException
import com.etermax.kata.domain.model.MoveResult
import com.etermax.kata.domain.model.Movement
import com.etermax.kata.domain.model.Rover
import com.etermax.kata.providers.RoverProvider

class Move(private val roverProvider: RoverProvider) {
    operator fun invoke(movements: List<String>): MoveResult {
        val rover = roverProvider.provide()
        val movements = movements.map { Movement.fromValue(it) }
            .toList()

        return try {
            val result = move(rover, movements)
            MoveResult.Success(result.position, movements)
        } catch (exception: ObstacleFoundException) {
            val rover = exception.rover
            val pendingMovements = exception.pendingMovements
            val movementsDone = movements.subList(0, movements.size - pendingMovements.size)
            MoveResult.Failure(rover.position, movementsDone, exception.message!!)
        }

    }

    private fun move(rover: Rover, movements: List<Movement>): Rover {
        return when {
            movements.isEmpty() -> rover
            else ->
                try {
                    move(rover.move(movements[0]), movements.subList(1, movements.size))
                } catch (exception: Throwable) {
                    throw ObstacleFoundException(rover, movements)
                }
        }
    }

}
