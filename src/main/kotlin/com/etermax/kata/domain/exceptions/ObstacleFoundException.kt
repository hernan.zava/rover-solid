package com.etermax.kata.domain.exceptions

import com.etermax.kata.domain.model.Movement
import com.etermax.kata.domain.model.Rover

class ObstacleFoundException(val rover: Rover, val pendingMovements: List<Movement>) : Throwable("Obstacle Found")
