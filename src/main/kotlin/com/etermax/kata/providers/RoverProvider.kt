package com.etermax.kata.providers

import com.etermax.kata.domain.model.Position
import com.etermax.kata.domain.model.Rover
import com.etermax.kata.domain.repository.RoverRepository

class RoverProvider(private val roverRepository: RoverRepository) {

    fun provide(): Rover {
        return roverRepository.get() ?: Rover(Position(), roverRepository)
    }

}
