package com.etermax.kata

import com.etermax.kata.domain.usecases.Move
import com.etermax.kata.infrastructure.InMemoryRoverRepository
import com.etermax.kata.providers.RoverProvider
import com.etermax.kata.resources.MoveResource
import spark.Spark.post

fun main(args: Array<String>) {

    val roverRepository = InMemoryRoverRepository()
    val roverProvider = RoverProvider(roverRepository)
    val moveRover = Move(roverProvider)
    val roverResource = MoveResource(moveRover)

    post("/move", roverResource::move)
}

