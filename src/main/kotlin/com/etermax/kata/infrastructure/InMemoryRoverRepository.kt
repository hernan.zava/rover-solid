package com.etermax.kata.infrastructure

import com.etermax.kata.domain.model.Rover
import com.etermax.kata.domain.repository.RoverRepository

class InMemoryRoverRepository : RoverRepository {

    private var rover: Rover? = null

    override fun store(rover: Rover) {
        this.rover = rover.copy()
    }

    override fun get(): Rover? {
        return rover?.copy()
    }
}