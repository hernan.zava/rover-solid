package com.etermax.kata.resources

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.etermax.kata.domain.model.MoveResult
import com.etermax.kata.domain.model.Movement
import com.etermax.kata.domain.model.Position
import com.etermax.kata.domain.usecases.Move
import spark.Request
import spark.Response
import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_OK

class MoveResource(private val move: Move) {
    fun move(request: Request, response: Response): String {
        val jsonObjectRequest = Json.parse(request.body()).asObject()
        val movements = jsonObjectRequest.get("movements").asArray()

        return when (val result = move(movements.map { it.asString() }.toList())) {
            is MoveResult.Success -> prepareOKResponse(response, result)
            is MoveResult.Failure -> prepareFailureResponse(response, result)
        }
    }

    private fun prepareFailureResponse(response: Response, result: MoveResult.Failure): String {
        val position = result.position
        val movements = result.movements
        val error = result.error

        response.status(HTTP_BAD_REQUEST)
        return JsonObject().add("roverPosition", toJson(position))
            .add("roverMovements", toJson(movements))
            .add("error", error)
            .toString()
    }

    private fun prepareOKResponse(response: Response, result: MoveResult.Success): String {
        val position = result.position
        val movements = result.movements

        response.status(HTTP_OK)
        return JsonObject().add("roverPosition", toJson(position))
            .add("roverMovements", toJson(movements))
            .toString()
    }

    private fun toJson(movements: List<Movement>): JsonArray {
        val jsonArray = JsonArray()
        movements.forEach { jsonArray.add(it.value) }
        return jsonArray
    }

    private fun toJson(position: Position): JsonObject {
        val direction = position.direction
        return JsonObject().add("x", position.x)
            .add("y", position.y)
            .add("direction", direction.value)
    }
}
