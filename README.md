### Bienvenido al proyecto ROVER de la NASA.

Fuiste seleccionado, de entre un montón de candidatos, para que mejores el sistema
de navegación del Mars ROVER. 

Tus conocimientos sobre SOLID y tu predisposición a encarar desafíos de manera fullstack fueron los motivos por los cuales nos decidimos a contratarte.

Actualmente contamos con una solución que funciona, pero el ingeniero que la desarrolló fue despedido (sus ideas sobre vietnam... además se robaba los proyectores de las salas), con lo cual no tenemos a quien preguntar sobre como esta hecho ni que se pretendía hacer. Lo único con lo que contamos es con el código fuente. 

Sabemos que el código tiene test, lo que no sabemos es la cantidad de cobertura del mismo. 

Tu trabajo es el siguiente:

1. "SOLIDIFICAR" la solución. Sabemos que no está desarrollado de la mejor manera posible. Sabemos que los movimientos del rover son pocos y tenemos que ampliarlos, pero también sabemos que hacerlo implicaría modificar cosas que HOY funcionan correctamente. 

1. Necesitamos que cada vez que se genere una nueva versión del software, que automáticamente se haga un tag en el repositorio, ya que si tenemos que hacer un rollback de un deploy, tiene que poder hacerse de manera rápida. Hoy contamos con una sola rama (master) y nada más. 

1. El sistema operativo del ROVER fue actualizado por nuestros especialistas y ahora cuenta con docker. Para sacar provecho de la situación, queremos que cada nueva versión sea dockerizada y publicada en nuestro registry. No tenemos idea de como se hace eso, pero nuestros especialistas de SRE nos pueden dar una mano de ser necesario. Tener en cuenta que todo lo desarrollado no puede salir de nuestros dominios, con lo cual debemos trabajar con las herramientas que nos ofrece gitlab en nuestro grupo.

1. Queremos poder probar la solución desarrollada en ambientes de test para hacer pruebas de regresión (no podemos arriesgarnos a deployar una solución directamente en el SO del ROVER con un bug). Por tal motivo necesitamos tener algún servidor en la nube, similar a las prestaciones del rover (1 GB de ram y 1 CPU), que cuente con docker para hacer el deploy de la imagen. Cómo es información sensible, solo las personas autorizadas deben poder llegar a esos endpoints. La creación de estas instancias puede hacerse a mano, pero el deployado de la solución en ellas lo necesitamos automatizado y a la mano de cualquiera. 

Un esquema que se nos ocurre es el siguiente:
- branch: release_X.X.X
    - stages: build, test, deploy (automático)

- branch: dev
    - stages: build, test

- branch: master
    - stages: build, test, tag

- tags:
    - stages: build, test, deploy (a demanda)

Es esto posible?? Escuchamos otras propuestas. 

Saludos y bienvenido




